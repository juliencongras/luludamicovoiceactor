const playerButton = document.querySelectorAll('.PlayButton');

playerButton.forEach(
    btn => {
        const audio = document.getElementById('audio'+btn.getAttribute('data-audio'))
        const timeline = document.getElementById('timeline'+btn.getAttribute('data-audio'))
        btn.addEventListener('click', e =>{
            toggleAudio(audio, btn)
        })
          
        audio.onended = function() {
            btn.style.borderTop = "10px solid transparent";
            btn.style.borderBottom = "10px solid transparent";
            //Switch back to the play state with style here
        }

          
        audio.ontimeupdate = function() {
            const percentagePosition = (100*audio.currentTime) / audio.duration;
            timeline.style.backgroundSize = `${percentagePosition}% 100%`;
            timeline.value = percentagePosition;
        }
        
        timeline.addEventListener('change', function() {
            const time = (timeline.value * audio.duration) / 100;
            audio.currentTime = time;
        });
    }
)


//const audio = document.querySelector('audio');
//const timeline = document.querySelector('.timeline');

function toggleAudio (audio, playerButton) {
    if (audio.paused) {
        audio.play();
        playerButton.style.borderTop = "10px solid white";
        playerButton.style.borderBottom = "10px solid white";
        //Put the style changes of the player button here
    } else {
        audio.pause();
        playerButton.style.borderTop = "10px solid transparent";
        playerButton.style.borderBottom = "10px solid transparent";
        //Put the style changes of the player button here
    }
}