---
layout: index.html
bigTextFirstPicture:
    Professional, Versatile & passionate
smallTextFirstPicture:
    Lulu's voice encompasses the warmth, clarity and charisma that your project deserves.
presentationBigText:
    With over 10 years of professional vocal work
    Lulu is excited to be offering her services as a freelance Vocal Artist.
presentationSmallText:
    After Graduating with a Degree in Musical Theatre from The London College of Music in 2011,
    Lulu realised that her true passion lay within Vocal Work. From Voice Acting to Audio Script
    Writing to Singing... Commercials to Animated Movies to Corporate Video's...
    Lulu is always excited to experiment with pitch, tone and rhythm to bring the employer exactly
    what they are looking for.
quotes:
    -
        testimonialQuote:
            She is a lovely person, very kind and extremely
            talented!! It's a pleasure working with her!
            
        quotee:
            Indira Zuleta, The Tiger Girl Story.
        bgImage:
            ../ressources/images/2.jpeg
    
    -
        testimonialQuote:
            I have known and worked with Lulu for the past two years both as a session singer and as a vocalist on our projects for FRESHA Records.
            I can truly say Lulu is a consummate professional - a great vocalist, professional, hardworking, creative and very talented. She understands the workings of the voice and is a true joy to work with.
        quotee:
            Dave Morgan, Fresha Records.
        bgImage: 
            ../ressources/images/3.jpg

firstImage: ../ressources/images/1.jpg

websiteName:
    Lulu D'Amico - Voice Actor
home: 
    Home 
vocal: 
    Vocal Demo's
contact:
    Contact
pageName:
    Homepage
titleContact:
    Contact
mailText:
    Email
contactMail:
    Ocimadalia@Gmail.com
textMandyProfile:
    Mandy Profile
mandyProfile:
    https://www.mandy.com/uk/v/lulu-damico
ukPhone1:
    +447843824367
ukPhone2:
    +44 7843824367
ukPhoneText:
    UK Phone
frPhone1:
    +33638218791
frPhone2:
    +33 638218791
frPhoneText:
    French Phone

---